# Copyright 2018-2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import fnmatch
import os
import re
import functools
import traceback

from django.utils.dateparse import parse_datetime
from django.db.models import Count
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from ..models import Author, Commit, File, FileChange, Repository
from . import commands

# we use git --log with a special one-line format string to capture certain fields
# we regex across those fields with a custom delimiter to make it easy to find them

DEL = '&DEL&>'

# Fields recorded (in order)
# commit hash %H
# author_name %an
# author_date %ad
# commit_date %cd
# author_email %ae
# author_name %an
# subject %f

PRETTY_STRING = f"'{DEL}%H{DEL}%an{DEL}%ad{DEL}%cd{DEL}%ae{DEL}%f{DEL}'"

# the regex to match the string, which must watch the log format PRETTY_STRING

PARSER_RE_STRING = f"{DEL}(?P<commit>.*){DEL}(?P<author_name>.*){DEL}(?P<author_date>.*){DEL}(?P<commit_date>.*){DEL}(?P<author_email>.*){DEL}(?P<subject>.*){DEL}"

PARSER_RE = re.compile(PARSER_RE_STRING, re.VERBOSE)

FILES_HACK_REPO = None
FILES_HACK = dict()

# Regex for handling arbitrary file path renames;
# `(/)?`: First captured group that matches `/` optionally
# `(?:\{[^}=]+=>)`: non-captured group to match until `=>`
# `([^}]+)`: matches the portion upto next `}` (`\}`)
# In the replacement, only the captured groups are used.
FILE_PATH_RENAME_RE = re.compile(r'(/)?(?:\{[^}=]+=>)([^}]+)\}')


class Commits:

    """
    This class clones a repository (git) using a provided URL and credential
    and proceeds to execute git log on it to scan its data
    """

    @classmethod
    def get_file(cls, repo, path, filename):

        """
        provide a lookup of File objects for repo/path/filename tuples to
        prevent excessive database access. This cache is only kept around
        for the current repository
        """

        global FILES_HACK_REPO
        global FILES_HACK
        if FILES_HACK_REPO != repo.name:
            FILES_HACK = dict()
            FILES_HACK_REPO = repo.name
            files = File.objects.filter(repo=repo).all()
            for fobj in files:
                assert fobj is not None
                key = os.path.join(fobj.path, fobj.name)
                FILES_HACK[key] = fobj

        original = os.path.join(path, filename)
        result = FILES_HACK[original]
        assert result is not None
        return result

    @classmethod
    def bulk_create(cls, total_commits, total_files, total_file_changes):
        """
        we keep a list of the three types of objects and only create them periodically,
        to prevent from doing too many database transactions.  The batch size here is fairly
        arbitrary.
        """

        # by not ignoring conflicts, we can test whether our scanner "overwork" code is correct
        # use -F to try a full test from scratch
        if len(total_commits):
            Commit.objects.bulk_create(total_commits, 100, ignore_conflicts=True)
            del total_commits[:]
        if len(total_files):
            File.objects.bulk_create(total_files, 100, ignore_conflicts=True)
            del total_files[:]
        if len(total_file_changes):
            FileChange.objects.bulk_create(total_file_changes, 100, ignore_conflicts=True)
            del total_file_changes[:]

    @classmethod
    def process_commits(cls, repo, repo_dir, mode='Commit'):

        """
        Uses git log to gather the commit data for a repository.  This is run three times in three different
        modes over the same git log output.  See usage in processor.py.
        """

        cmd_string = 'git rev-list --all --count'
        commit_total = commands.execute_command(repo, cmd_string, log=False, timeout=600, chdir=repo_dir, capture=True)

        try:
            commit_total = int(commit_total)
        except TypeError:
            print("no commits yet")
            return

        cmd_string = ('git log --all --numstat --date=iso-strict-local --pretty=format:'
                      + PRETTY_STRING)


        last_commit = None
        count = 0
        total_commits = []
        total_file_changes = []
        total_files = []

        global GLITCH_COUNT

        def handler(line):
            """
            this code processes every line from the output
            """

            nonlocal last_commit
            nonlocal count

            if count % 200 == 0:
                print("scanning (repo:%s) (mode:%s): %s/%s" % (repo, mode, count, commit_total))
            if count % 2000 == 0:
                cls.bulk_create(total_commits, total_files, total_file_changes)

            if not line or line == "\n":
                return True

            elif line.startswith(DEL):

                commit = cls.handle_diff_information(repo, line, mode)
                if last_commit != commit:
                    count = count + 1
                    last_commit = commit
                    total_commits.append(commit)
                return True

            elif "does not have any commits yet" in line:
                #print("skipping, no commits yet")
                return False

            else:
                if mode != 'Commit':
                    assert last_commit is not None
                    cls.handle_file_information(repo, line, last_commit, mode, total_files, total_file_changes)
                return True


        commands.execute_command(repo, cmd_string, log=False, timeout=1200, chdir=repo_dir, handler=handler)
        cls.bulk_create(total_commits, total_files, total_file_changes)

        return True

    @classmethod
    def process_stray_merge_commits(cls, repo, work_dir):

        """
        There are apparent issues with the 'git log' command that do not show file changes for commits
        from merge commits when the *source* of the merge commit is no longer in the tree.  Thus we attempt
        to find all commits *without* file changes, and run 'git show' on them.

        This code is significantly LESS well organized than the rest of the scanner, as a consequence of being a fix
        to the existing streamlined logic to just run through 'git log' a few times, adding commits, then files, then
        file changes.  We'll put up with it being slower as most repos should NOT be full of a lot of merge
        commits, but may want to consider improving this to be less dumb in the future - particularly, it bypasses
        the FILES_HACK cache above.
        """

        if not repo.organization.include_merge_commits:
            print("* parsing merge commits is disabled")
            return

        commits = Commit.objects.filter(repo=repo).annotate(file_change_count=Count('file_changes')).filter(file_change_count=0)
        commit = None
        files = []
        file_changes = []


        def parse_git_show(line):
            nonlocal commit

            added = None
            removed = None
            path = None
            moved = False


            try:
                tokens = line.split()
                added = int(tokens[0])
                removed = int(tokens[1])
                path = " ".join(tokens[2:])
                (moved, path) = cls.repair_move_path(path)

            except:
                # FIXME: catch specific error types

                # print(line)
                # traceback.print_exc()
                # print("~~")
                return True

            if not cls.should_process_path(repo, path):
                # print("~~~")
                return True

            else:

                fname = os.path.basename(path)
                (_, ext) = os.path.splitext(path)
                path = os.path.dirname(path)

                file = None
                try:
                    file = File.objects.get(
                        repo=repo,
                        path=path,
                        name=fname,
                        ext=ext
                    )
                except ObjectDoesNotExist:
                    file = File(
                        repo=repo,
                        path=path,
                        name=fname,
                        ext=ext,
                        binary=False, # FIXME: we're not really using this, but this is wrong :)
                        created_by=commit
                    )
                    file.save()

                file_change = cls.get_file_change(commit, path, fname, added, removed, moved=moved, file=file)
                try:
                    file_change.save()
                except IntegrityError:
                    # FIXME: already exists, should have looked for it first
                    pass
                return True

        count = commits.count()
        item = 0

        for x in commits:
            item = item + 1
            print("repairing %s/%s merge commits" % (item, count))
            commit = x
            cmd_string = "git show %s --numstat -m" % commit.sha
            commands.execute_command(repo, cmd_string, log=False, timeout=20, chdir=work_dir, handler=parse_git_show)
            # print("%s is short" % x)



        #raise Exception("STOP!")


    @classmethod
    def get_file_change(cls, commit, path, fname, lines_added, lines_removed, moved=False, file=None):

        if file is None:
            file = cls.get_file(commit.repo, path, fname)

        if file is None:
            # this shouldn't happen, but if we get here the parser has a bug.
            raise Exception("FATAL, MISSING FILE RECORD, SHOULDN'T BE HERE!")

        # these look like they should be booleans, but these are kept this way for fast aggregations
        is_create = 1
        is_move = 0
        is_edit = 0

        assert file.created_by is not None, "file record does not record the creating commit, please rescan once with -F"

        # older scans won't have this and will need to do a rescan
        if file.created_by.sha != commit.sha:
            is_edit = 1
            is_create = 0
        if moved:
            is_move = 1

        return FileChange(
            commit=commit,
            lines_added=lines_added,
            lines_removed=lines_removed,
            file=file,
            is_create=is_create,
            is_move=is_move,
            is_edit=is_edit
        )

    @classmethod
    def create_file(cls, full_path, commit, la, lr, binary, mode, total_files, total_file_changes, moved):
        """
        After we have recorded commits, this function creates either Files or FileChange objects
        depending on what scanner pass we are running through.
        """

        assert commit is not None
        assert mode in [ 'File', 'FileChange' ]

        fname = os.path.basename(full_path)

        # find the extension
        (_, ext) = os.path.splitext(full_path)
        path = os.path.dirname(full_path)

        if mode == 'File':

            # update the global file object with the line counts

            assert commit is not None

            total_files.append(File(
                repo=commit.repo,
                path=path,
                name=fname,
                ext=ext,
                binary=binary,
                created_by=commit
            ))

            # BOOKMARK

        elif mode == 'FileChange':
            file_change = cls.get_file_change(commit, path, fname, la, lr, moved=moved)
            total_file_changes.append(file_change)



    @classmethod
    def matches(self, needle, haystack, exact=False, trim_dot=False):

        """
        This function is used by the source code filtering feature to see if a file path
        matches an expression or not.
        """

        #  user input may be inconsistent about trailing slashes so be flexible
        if haystack.endswith("/"):
            haystack = haystack[:-1]
        if needle.endswith("/"):
            needle = needle[:-1]

        if trim_dot:
            # for extension checking, do not require the user input to be ".mp4" to mean "mp4"
            haystack = haystack.replace(".","")
            needle = needle.replace(".", "")

        if "?" in needle or "*" in needle or "[" in needle:
            # this looks like a fnmatch pattern
            return fnmatch.fnmatch(haystack, needle)
        elif exact:
            # we are processing an extension, require an exact match


            return haystack == needle
        else:
            # we are processing paths, not extensions, so just require it to start with the substring
            return haystack.startswith(needle)

    @classmethod
    def has_matches(cls, needles, haystack, exact=False, trim_dot=False):
        """
        tests whether a file pattern has any one of multiple matches
        """

        for needle in needles:
            if cls.matches(needle, haystack, exact=exact, trim_dot=trim_dot):
                return True
        return False

    @classmethod
    def has_no_matches(cls, needles, haystack, exact=False, trim_dot=False):
        return not cls.has_matches(needles, haystack, exact=exact, trim_dot=trim_dot)

    @classmethod
    def repair_move_path(cls, path):
        """
        handles details about moves in git log by fixing path elements like /{org=>com}/
        to just log the file in the final path. This will possibly give users credit for
        aspects of a move but this something we can explore later. Not sure if it does - MPD.
        """
        moved = False


        # some regex issues, doing this 'dumb' for now.

        # /a/b/c => /d/e/f ----- possibly appears, not sure
        # /a/bc/{d => e }/g  --- this definitely appears

        if "=>" in path:
            (left, right) = path.split("=>")
            if "{" in left and "}" in right:
                (left1, left2) = left.split("{")
                right = right.replace("}","")
                resolved = "%s%s" % (left1.strip(), right.strip())
                return (True, resolved)
            else:
                return (True, right.strip())

        #if FILE_PATH_RENAME_RE.match(path):
        #    return (True, FILE_PATH_RENAME_RE.sub(r'\1\2', path))

        else:
            return (False, path)

    @classmethod
    def should_process_path(cls, repo, path):
        """
        Repository configuration supports filtering based on path, to decide to index or not-index
        certain files.  This might be used to only index a 'src/' directory or otherwise not
        index a directory called 'docs/', and is off by default.  This function handles
        a decision on whether to process a path.
        """

        org = repo.organization
        directory_allow = repo.scanner_directory_allow_list or org.scanner_directory_allow_list
        directory_deny  = repo.scanner_directory_deny_list or org.scanner_directory_deny_list

        extension_allow = repo.scanner_extension_allow_list or org.scanner_extension_allow_list
        extension_deny = repo.scanner_extension_deny_list or org.scanner_extension_deny_list

        dirname = os.path.dirname(path)
        split_ext = os.path.splitext(path)

        extension = None
        if len(split_ext) > 1:
            extension = split_ext[-1]

        if directory_allow:
            directory_allow = directory_allow.split("\n")
        if directory_deny:
            directory_deny = directory_deny.split("\n")
        if extension_allow:
            extension_allow = extension_allow.split("\n")
        if extension_deny:
            extension_deny = extension_deny.split("\n")


        if directory_allow and cls.has_no_matches(directory_allow, dirname):
            return False

        if directory_deny and cls.has_matches(directory_deny, dirname):
            return False

        if extension:
            if extension_allow and cls.has_no_matches(extension_allow, extension, exact=True, trim_dot=True):
                return False

            if extension_deny and cls.has_matches(extension_deny, extension, exact=True, trim_dot=True):
                return False

        return True

    @classmethod
    def handle_file_information(cls, repo, line, last_commit, mode, total_files, total_file_changes):

        """
        process the list of file changes in this commit
        """

        assert repo is not None
        assert last_commit is not None

        tokens = line.split()
        (added, removed, path) = (tokens[0], tokens[1], ''.join(tokens[2:]))


        # binary files will have '-' in their field changes. Set these to 0
        binary = False
        if added == '-':
            binary = True
            added = 0
        if removed == '-':
            binary = True
            removed = 0

        # FIXME: when scanning one repo, the added string containted

        try:
            added = int(added)
            removed = int(removed)
        except:
            # FIXME:
            # I found one instance in one repo where the 'added' text returns "warning: inexact" and in this case
            # we might as well keep going, we probably need to parse the line differently in this instance.
            # example found in kubernetes/kubernetes on github. This reference is not an endorsement.
            added = 0
            removed = 0

        (moved, path) = cls.repair_move_path(path)

        if not cls.should_process_path(repo, path):
            return None

        cls.create_file(path, last_commit, added, removed, binary, mode, total_files, total_file_changes, moved)


    @classmethod
    @functools.lru_cache(maxsize=10000, typed=False)
    def get_author(cls, repo, email, author_name):
        maybe = Author.objects.filter(email=email).all()
        if len(maybe):
            author = maybe.first()
            count = 0
            # if we found an alias record, walk up the tree until we find the root author, because
            # they get the stats.
            while author.alias_for:
                author = author.alias_for
                count = count + 1
                if count > 3:
                    print("warning: author aliases are too deep")
                    break
            found = author

            if not found.display_name:
                # update database old author records if a name is now available
                found.display_name = author_name
                print("updating author name (%s) -> (%s)" % (found.email, found.display_name))
                found.save()
            return found
        else:
            return Author.objects.create(email=email, display_name=author_name)

    @classmethod
    def handle_diff_information(cls, repo, line, mode):

        """
        process the amount of lines changed in this commit
        """


        # FIXME: give all these fields names
        match = PARSER_RE.match(line)
        if not match:
            raise Exception("DOESN'T MATCH? %s" % line)
        data = match.groupdict()
        if mode != 'Commit':
            # running back through the logs to set up the file changes
            commit = Commit.objects.get(repo=repo, sha=data['commit'])
            return commit

        email = data['author_email']
        author_name = data['author_name']
        author = cls.get_author(repo, email, author_name)

        commit_date = parse_datetime(data['commit_date'])
        author_date = parse_datetime(data['author_date'])

        # will pass on to bulk_create
        return Commit(
            sha=data['commit'],
            subject=data['subject'],
            repo=repo,
            author=author,
            author_date=author_date,
            commit_date=commit_date
        )
